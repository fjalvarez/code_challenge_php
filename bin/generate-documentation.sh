#!/usr/bin/env bash
DIR=$( cd "$( dirname "$0" )" && cd .. && pwd )

cd $DIR

$DIR/bin/docker-runner.sh vendor/bin/phpdoc run -d app -t docs/api

