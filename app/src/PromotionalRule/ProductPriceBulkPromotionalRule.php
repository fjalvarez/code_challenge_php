<?php

namespace CabifyChallenge\PromotionalRule;

use CabifyChallenge\Model\Item;
use CabifyChallenge\Model\PromotionalRule\MinimumItemPromotionalRule;
use CabifyChallenge\Model\PromotionalRule\ProductPriceBulkPromotionalRuleInterface;

/**
 * Class ProductPriceBulkPromotionalRule
 */
class ProductPriceBulkPromotionalRule extends MinimumItemPromotionalRule
{
    /**
     * {@inheritdoc}
     */
    public function needApply($cart)
    {
        $item = $cart->get($this->getItemCode());
        if (!empty($item) && $item->getQuantity() >= $this->getMinNumberItems()) {
            return true;
        }
        return false;
    }

    /**
     * @param Item $item
     * @return mixed
     */
    public function getDiscountedPrice(Item $item)
    {
        return $this->getValue();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return sprintf('ProductPriceBulk_%s_%d', $this->getItemCode(), $this->getMinNumberItems());
    }
}