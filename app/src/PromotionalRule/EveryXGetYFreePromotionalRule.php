<?php

namespace CabifyChallenge\PromotionalRule;

use CabifyChallenge\Model\Item;
use CabifyChallenge\Model\PromotionalRule\FreeItemsPromotionalRule;
use CabifyChallenge\Model\PromotionalRule\MinimumItemPromotionalRule;

/**
 * Class EveryXGetYFreePromotionalRule
 */
class EveryXGetYFreePromotionalRule extends MinimumItemPromotionalRule
{
    /**
     * {@inheritdoc}
     */
    public function needApply($cart)
    {
        $item = $cart->get($this->getItemCode());
        if (!empty($item) && $item->getQuantity() >= $this->getMinNumberItems()) {
            return true;
        }
        return false;
    }

    /**
     * @param Item $item
     * @return mixed
     */
    public function getDiscountedPrice(Item $item)
    {
        $itemsFree = floor(($item->getQuantity() / $this->getMinNumberItems())) * $this->getValue();
        $totalPrice = $item->getQuantity() * $item->getPrice();
        $discount = ($item->getPrice() * $itemsFree);
        return ($totalPrice - $discount) / $item->getQuantity();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return sprintf('EveryXgetYfree_%s_%d', $this->getItemCode(), $this->getMinNumberItems());
    }
}