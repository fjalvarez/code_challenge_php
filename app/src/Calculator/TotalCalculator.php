<?php

namespace CabifyChallenge\Calculator;

use CabifyChallenge\Model\Item;

/**
 * Class TotalCalculator
 */
class TotalCalculator
{
    /**
     * @param array|Item[] $items
     *
     * @return int
     */
    public function calculate($items)
    {
        $total = 0;
        foreach ($items as $item) {
            $total += $item->getPrice() * $item->getQuantity();
        }
        return $total;
    }
}