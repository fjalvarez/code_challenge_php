<?php

namespace CabifyChallenge\Model\PromotionalRule;

/**
 * Class MinimumItemPromotionalRule
 */
abstract class MinimumItemPromotionalRule extends PromotionalRule
{
    /**
     * @var integer
     */
    protected $minNumberItems;

    /**
     * @return int
     */
    public function getMinNumberItems()
    {
        return $this->minNumberItems;
    }

    /**
     * @param int $minNumberItems
     * @return MinimumItemPromotionalRule
     */
    public function setMinNumberItems($minNumberItems)
    {
        $this->minNumberItems = $minNumberItems;
        return $this;
    }

}