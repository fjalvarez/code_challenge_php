<?php

namespace CabifyChallenge\Model\PromotionalRule;

/**
 * Interface PromotionalRuleInterface
 */
interface PromotionalRuleInterface
{
    /**
     * @return string
     */
    public function getItemCode();

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getValue();
}