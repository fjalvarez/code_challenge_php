<?php

namespace CabifyChallenge\Model\PromotionalRule;

use CabifyChallenge\Container\CartContainer;

/**
 * Interface PromotionalRuleApplyInterface
 */
interface PromotionalRuleApplyInterface
{
    public function needApply($cart);

    public function apply(CartContainer $cart);
}