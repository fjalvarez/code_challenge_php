<?php

namespace CabifyChallenge\Model\PromotionalRule;

use CabifyChallenge\Container\CartContainer;
use CabifyChallenge\Model\Item;

/**
 * Class PromotionalRule
 */
abstract class PromotionalRule implements PromotionalRuleInterface, PromotionalRuleApplyInterface
{
    /**
     * @var float
     */
    protected $value;

    /**
     * @var string
     */
    protected $itemCode;

    /**
     * @return string
     */
    public function getItemCode()
    {
        return $this->itemCode;
    }

    /**
     * @param string $itemCode
     * @return $this
     */
    public function setItemCode($itemCode)
    {
        $this->itemCode = $itemCode;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return PromotionalRule
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @param CartContainer $cart
     *
     * @return bool
     */
    abstract public function needApply($cart);

    /**
     * @param Item $item
     * @return mixed
     */
    abstract public function getDiscountedPrice(Item $item);

    /**
     * @param CartContainer $cart
     *
     * @return $this
     */

    /**
     * @param CartContainer $cart
     *
     * @return $this
     */
    public function apply(CartContainer $cart)
    {
        $item = $cart->get($this->getItemCode());
        $item->setPrice($this->getDiscountedPrice($item));
        $cart->calculateTotal();
        return $this;
    }
}