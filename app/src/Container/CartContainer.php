<?php

namespace CabifyChallenge\Container;

use CabifyChallenge\Calculator\TotalCalculator;
use CabifyChallenge\Model\Item;
use CabifyChallenge\Model\PromotionalRule\PromotionalRuleApplyInterface;

/**
 * Class CartContainer
 */
class CartContainer
{
    /**
     * @var array|Item[]
     */
    protected $cart;

    /**
     * @var TotalCalculator
     */
    protected $totalCalculator;

    /**
     * @var float
     */
    protected $totalPrice;

    /**
     * @var PromotionalRuleContainer $promotionalRules
     */
    private $promotionalRules;

    /**
     * @var Item $products
     */
    private $products;

    /**
     * CartContainer constructor.
     */
    public function __construct()
    {
        $this->cart = array();
        $this->promotionalRules = new PromotionalRuleContainer();
        $this->products = new ProductContainer();
        $this->totalCalculator = new TotalCalculator();
    }

    /**
     * @param string $code
     *
     * @return Item
     */
    public function get($code)
    {
        return $this->cart[$code];
    }

    /**
     * @param string $code
     *
     * @return $this
     */
    public function add($code)
    {
        if (!in_array($code, $this->cart)) {
            $this->cart[$code] = $this->products->get($code);
        }
        $this->cart[$code]->increaseQuantity();
        $this->calculateTotal();
        return $this;
    }

    /**
     * @return $this
     */
    public function calculateTotal()
    {
        $this->totalPrice = $this->totalCalculator->calculate($this->cart);
        return $this;
    }

    /**
     * Apply promotional rules
     *
     * @return $this
     */
    public function applyPromotionalRules()
    {
        foreach ($this->promotionalRules->getAll() as $promotionalRule) {
            if ($promotionalRule instanceof PromotionalRuleApplyInterface) {
                if ($promotionalRule->needApply($this)) {
                    $promotionalRule->apply($this);
                }
            }
        }
        return $this;
    }

    /**
     * @return array|Item[]
     */
    public function getAll()
    {
        return $this->cart;
    }

    /**
     * @param float $total
     *
     * @return $this
     */
    public function setTotal($total)
    {
        $this->totalPrice = $total;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->totalPrice;
    }
}