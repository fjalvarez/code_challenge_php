<?php

namespace CabifyChallenge\Container;

use CabifyChallenge\Model\Item;
use CabifyChallenge\Model\Product;
use InvalidArgumentException;
use Symfony\Component\Yaml\Yaml;

/**
 * Class ProductContainer
 */
class ProductContainer
{
    const PRODUCT_CONTAINER_CONFIG_FILE =  __DIR__ . '/../../config/products.yml';

    /**
     * @var Product[]
     */
    protected $products;

    /**
     * ProductContainer constructor.
     */
    public function __construct()
    {
        $this->products = $this->initializeProducts();
    }

    /**
     * @param string $code
     *
     * @return Product
     */
    public function get($code)
    {
        if (empty($this->products[$code])) {
            throw new InvalidArgumentException("Invalid product $code");
        }
        return $this->products[$code];
    }

    /**
     * @return array
     */
    protected function initializeProducts()
    {
        $products = Yaml::parse(self::PRODUCT_CONTAINER_CONFIG_FILE);
        $result = array();
        foreach ($products['products'] as $code => $product) {
            $result[$code] = new Item();
            $result[$code]
                ->setCode($code)
                ->setName($product['name'])
                ->setPrice($product['price']);
        }
        return $result;
    }
}