<?php

namespace CabifyChallenge\Container;

use CabifyChallenge\Model\PromotionalRule\PromotionalRule;
use Symfony\Component\Yaml\Yaml;

/**
 * Class PromotionalRuleContainer
 */
class PromotionalRuleContainer
{
    const PROMOTIONAL_RULE_CONTAINER_CONFIG_FILE =  __DIR__ . '/../../config/promotions.yml';

    /**
     * @var array|PromotionalRule[]
     */
    protected $promotionalRules;

    /**
     * PromotionalRuleContainer constructor.
     */
    public function __construct()
    {
        $this->promotionalRules = $this->initializePromotionalRules();
    }

    /**
     * @param string $name
     *
     * @return PromotionalRule
     */
    public function get($name)
    {
        return $this->promotionalRules[$name];
    }

    /**
     * @return array|PromotionalRule[]
     */
    public function getAll()
    {
        return $this->promotionalRules;
    }

    /**
     * @return array
     */
    protected function initializePromotionalRules()
    {
        $promotionalRules = Yaml::parse(self::PROMOTIONAL_RULE_CONTAINER_CONFIG_FILE);
        $result = array();
        foreach ($promotionalRules['promotions'] as $promotionalRule) {
            $promotionalRuleName = "CabifyChallenge\PromotionalRule\\" . $promotionalRule['type'];
            if (class_exists($promotionalRuleName)) {
                $newPromotionalRule = new $promotionalRuleName();
                $newPromotionalRule
                    ->setItemCode($promotionalRule['item'])
                    ->setMinNumberItems($promotionalRule['minNumItems'])
                    ->setValue($promotionalRule['value']);
                $result[$newPromotionalRule->getName()] = $newPromotionalRule;
            } else {
                throw new \InvalidArgumentException('Invalid promotion');
            }
        }
        return $result;
    }
}