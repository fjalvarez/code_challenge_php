<?php

namespace CabifyChallenge\Checkout;

use CabifyChallenge\Container\CartContainer;

/**
 * Class Checkout
 */
class Checkout implements CheckoutInterface
{
    /**
     * @var CartContainer[]
     */
    private $cart;

    /**
     * {@inheritdoc}
     */
    public function __construct()
    {
        $this->cart = new CartContainer();
    }

    /**
     * {@inheritdoc}
     */
    public function scan($code)
    {
        $this->cart->add($code);
    }

    /**
     * return array|Item[]
     */
    public function getItems()
    {
        return $this->cart->getAll();
    }

    /**
     * {@inheritdoc}
     */
    public function total()
    {
        $this->cart->applyPromotionalRules();
        $this->cart->calculateTotal();
        return round($this->cart->getTotal(), 2);
    }
}