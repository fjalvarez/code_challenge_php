<?php

namespace CabifyChallenge\Checkout;

use CabifyChallenge\Container\PromotionalRuleContainer;
use CabifyChallenge\Model\Item;

/**
 * Interface CheckoutInterface
 */
interface CheckoutInterface
{
    /**
     * CheckoutInterface constructor.
     */
    public function __construct();

    /**
     * @param string $code
     *
     * @return $this
     */
    public function scan($code);

    /**
     * @return float
     */
    public function total();
}