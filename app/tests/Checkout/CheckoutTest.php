<?php

namespace CabifyChallengeTest\Checkout;

use CabifyChallenge\Checkout\Checkout;
use CabifyChallenge\Container\PromotionalRuleContainer;
use CabifyChallenge\Model\Item;
use CabifyChallengeTest\TestCaseHelper;

/**
 * Class CheckoutTest
 */
class CheckoutTest extends TestCaseHelper
{
    /**
     * Test checkout class
     */
    public function testCheckout()
    {
        $checkout = new Checkout();
        $expected = array();

        $checkout->scan($this->getVoucher()->getCode());
        $expected[$this->getVoucher()->getCode()] = $this->getVoucher()->increaseQuantity();

        $this->assertEquals($expected, $checkout->getItems());

        $checkout->scan($this->getTshirt()->getCode());
        $expected[$this->getTshirt()->getCode()] = $this->getTshirt()->increaseQuantity();

        $this->assertEquals($expected, $checkout->getItems());

        $total = 0;
        /** @var Item $item */
        foreach ($expected as $item) {
            $total += $item->getPrice();
        }

        $this->assertEquals($total, $checkout->total());
    }
}