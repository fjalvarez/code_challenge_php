<?php

namespace CabifyChallengeTest\PromotionalRule;

use CabifyChallenge\Container\CartContainer;
use CabifyChallenge\PromotionalRule\EveryXGetYFreePromotionalRule;
use CabifyChallenge\Model\Item;
use CabifyChallengeTest\TestCaseHelper;

/**
 * Class ProductPriceBulkPromotionalRuleTest
 */
class EveryXGetYFreePromotionalRuleTest extends TestCaseHelper
{
    /**
     * @dataProvider getNeedApplyProvider
     */
    public function testNeedApply($minNumberItems, $quantity, $expectedResult)
    {
        $item = new Item();
        $item
            ->setCode('001')
            ->setQuantity($quantity);

        $cartContainer = \Phake::mock(CartContainer::class);
        \Phake::when($cartContainer)->get(\Phake::anyParameters())->thenReturn($item);

        $everyXgetYfree = new EveryXGetYFreePromotionalRule();
        $everyXgetYfree
            ->setMinNumberItems($minNumberItems)
            ->setItemCode($item->getCode());

        $this->assertEquals($expectedResult, $everyXgetYfree->needApply($cartContainer));
    }

    public function getNeedApplyProvider()
    {
        return array(
            array(
                'minNumItems' => 10,
                'quantity' => 1,
                'expectedResult' => false,
            ),
            array(
                'minNumItems' => 5,
                'quantity' => 5,
                'expectedResult' => true,
            ),
            array(
                'minNumItems' => 5,
                'quantity' => 6,
                'expectedResult' => true,
            ),
        );
    }

    /**
     * @dataProvider getGetDiscountedPriceProvider
     */
    public function testGetDiscountedPrice($price, $quantity, $minNumberItems, $freeItems, $discountedPrice)
    {
        $item = new Item();
        $item
            ->setCode('001')
            ->setPrice($price)
            ->setQuantity($quantity);
        
        $cartContainer = \Phake::mock(CartContainer::class);
        \Phake::when($cartContainer)->get(\Phake::anyParameters())->thenReturn($item);

        $productPriceBulkPromotionalRule = new EveryXGetYFreePromotionalRule();
        $productPriceBulkPromotionalRule
            ->setMinNumberItems($minNumberItems)
            ->setValue($freeItems)
            ->setItemCode($item->getCode());

        $this->assertEquals($discountedPrice, $productPriceBulkPromotionalRule->getDiscountedPrice($item));
    }

    public function getGetDiscountedPriceProvider()
    {
        return array(
            array(
                'price' => 5,
                'amount' => 2,
                'value' => 2,
                'free' => 1,
                'discountedPrice' => 2.5,
            ),
            array(
                'price' => 9,
                'amount' => 4,
                'value' => 2,
                'free' => 1,
                'discountedPrice' => 4.5,
            ),
            array(
                'price' => 4,
                'amount' => 4,
                'value' => 4,
                'free' => 3,
                'discountedPrice' => 1,
            ),
        );
    }
}