<?php

namespace CabifyChallengeTest\PromotionalRule;

use CabifyChallenge\Container\CartContainer;
use CabifyChallenge\PromotionalRule\ProductPriceBulkPromotionalRule;
use CabifyChallenge\Model\Item;
use CabifyChallengeTest\TestCaseHelper;

/**
 * Class ProductPriceBulkPromotionalRuleTest
 */
class ProductPriceBulkPromotionalRuleTest extends TestCaseHelper
{
    /**
     * @dataProvider getNeedApplyProvider
     */
    public function testNeedApply($minNumberItems, $quantity, $expectedResult)
    {
        $item = new Item();
        $item
            ->setCode('001')
            ->setQuantity($quantity);

        $cartContainer = \Phake::mock(CartContainer::class);
        \Phake::when($cartContainer)->get(\Phake::anyParameters())->thenReturn($item);

        $productPriceBulkPromotionalRule = new ProductPriceBulkPromotionalRule();
        $productPriceBulkPromotionalRule
            ->setMinNumberItems($minNumberItems)
            ->setItemCode($item->getCode());

        $this->assertEquals($expectedResult, $productPriceBulkPromotionalRule->needApply($cartContainer));
    }

    public function getNeedApplyProvider()
    {
        return array(
            array(
                'minNumItems' => 1,
                'quantity' => 0,
                'expectedResult' => false,
            ),
            array(
                'minNumItems' => 5,
                'quantity' => 5,
                'expectedResult' => true,
            ),
            array(
                'minNumItems' => 5,
                'quantity' => 6,
                'expectedResult' => true,
            ),
        );
    }

    public function testGetDiscountedPrice()
    {
        $minNumberItems = 10;
        $price = 10.0;

        $item = new Item();
        $item
            ->setCode('001')
            ->setPrice($price)
            ->setQuantity(1);

        $cartContainer = \Phake::mock(CartContainer::class);
        \Phake::when($cartContainer)->get(\Phake::anyParameters())->thenReturn($item);

        $productPriceBulkPromotionalRule = new ProductPriceBulkPromotionalRule();
        $productPriceBulkPromotionalRule
            ->setMinNumberItems($minNumberItems)
            ->setValue($price)
            ->setItemCode($item->getCode());

        $this->assertEquals($price, $productPriceBulkPromotionalRule->getDiscountedPrice($item));
    }
}