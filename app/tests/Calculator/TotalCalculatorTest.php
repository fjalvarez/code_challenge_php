<?php

namespace CabifyChallengeTest\Calculator;

use CabifyChallenge\Calculator\TotalCalculator;
use CabifyChallengeTest\TestCaseHelper;

/**
 * Class TotalCalculatorTest
 */
class TotalCalculatorTest extends TestCaseHelper
{
    public function testCalculate()
    {
        $expected = 32.5;

        $items = $this->getItems();
        foreach ($items as $item) {
            $item->increaseQuantity();
        }

        $totalCalculator = new TotalCalculator();
        $result = $totalCalculator->calculate($items);

        $this->assertEquals($expected, $result);
    }
}