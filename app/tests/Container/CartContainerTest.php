<?php

namespace CabifyChallengeTest\Container;

use CabifyChallenge\Container\CartContainer;
use CabifyChallenge\Model\Item;
use CabifyChallengeTest\TestCaseHelper;

/**
 * Class CartContainerTest
 */
class CartContainerTest extends TestCaseHelper
{
    public function testAdd()
    {
        $cartContainer = new CartContainer();
        $expected = array();

        $cartContainer->add($this->getVoucher()->getCode());
        $expected[$this->getVoucher()->getCode()] = $this->getVoucher()->increaseQuantity();
        $this->assertEquals($expected, $cartContainer->getAll());

        $cartContainer->add($this->getTshirt()->getCode());
        $expected[$this->getTshirt()->getCode()] = $this->getTshirt()->increaseQuantity();
        $this->assertEquals($expected, $cartContainer->getAll());

        $cartContainer->add($this->getTshirt()->getCode());
        $expected[$this->getTshirt()->getCode()]->increaseQuantity();
        $this->assertEquals($expected, $cartContainer->getAll());

        $cartContainer->calculateTotal();

        $total = 0;
        /** @var Item $item */
        foreach ($expected as $item) {
            $total += $item->getPrice() * $item->getQuantity();
        }

        $this->assertEquals($total, $cartContainer->getTotal());

        $expectedTotal = 42.42;
        $cartContainer->setTotal($expectedTotal);
        $this->assertEquals($expectedTotal, $cartContainer->getTotal());

        $cartContainer->applyPromotionalRules();
    }
}