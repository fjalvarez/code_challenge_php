<?php

namespace CabifyChallengeTest;

use CabifyChallenge\Model\Item;

/**
 * Class TestCaseHelper
 */
class TestCaseHelper extends \PHPUnit\Framework\TestCase
{
    protected $mug;
    protected $tshirt;
    protected $voucher;

    protected function setUp()
    {
        $this->voucher = new Item();
        $this->voucher
            ->setName('Cabify Voucher')
            ->setCode('VOUCHER')
            ->setPrice(5.0);

        $this->tshirt = new Item();
        $this->tshirt
            ->setName('Cabify T-Shirt')
            ->setCode('TSHIRT')
            ->setPrice(20);

        $this->mug = new Item();
        $this->mug
            ->setName('Cafify Coffee Mug')
            ->setCode('MUG')
            ->setPrice(7.5);
    }

    /**
     * @return array|Item
     */
    public function getItems()
    {
        return array(
            $this->getVoucher(),
            $this->getTshirt(),
            $this->getMug(),
        );
    }

    /**
     * @return Item
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

    /**
     * @return Item
     */
    public function getTshirt()
    {
        return $this->tshirt;
    }

    /**
     * @return Item
     */
    public function getMug()
    {
        return $this->mug;
    }
}