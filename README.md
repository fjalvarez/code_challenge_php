# Cabify Code Challengue

Create the image from Dockerfile

`docker build -t cabify_challengue .`

You have a runner to execute commands. For example, to install composer dependencies:

`./bin/docker-runner.sh composer install`

And to run the tests suite you have to create a copy of `phpunit.xml.dist` on app/tests folder and name it `phpunit.xml`. Then run

`./bin/run-tests.sh`

You can regenerate the documentation using

`./bin/generate-documentation.sh`

And to create an example of checkout process, just as follows:

```php
<?php
require_once 'vendor/autoload.php';

$checkout = new \CabifyChallenge\Checkout\Checkout();
$checkout->scan("VOUCHER");
$checkout->scan("TSHIRT");
$checkout->scan("VOUCHER");
$checkout->scan("VOUCHER");
$checkout->scan("MUG");
$checkout->scan("TSHIRT");
$checkout->scan("TSHIRT");

print($checkout->total() . PHP_EOL); // 74.5
```